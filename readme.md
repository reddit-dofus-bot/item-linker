# Dofus Encyclopedia bot for Reddit

This bot takes a look at the comments and submissions in the [Dofus subreddit](https://www.reddit.com/r/Dofus/), and replies with the stats of any item which is mentioned between [brackets].

## Getting Started
### Prerequisites

* Install [Git](https://git-scm.com/downloads)
* Install [Python](https://www.python.org/downloads/) (Python 3.6.4+)
* Create a [Reddit account](https://www.reddit.com/login) for the bot

### Installing

Before running the bot, several Python modules need to be installed. The following modules are used:

* [Praw](https://praw.readthedocs.io/en/latest/)
* [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [Requests](http://docs.python-requests.org/en/master/)
* [Google-Search-API](https://github.com/abenassi/Google-Search-API/)

These modules can be installed by entering the following lines in the command console:

```
install beautifulsoup4
install requests
install praw
install git+https://github.com/abenassi/Google-Search-API/
```

Log in to your [Reddit account](https://www.reddit.com/login) and go to `preferences` -> `apps` -> `create app`. Follow the instructions to create a 'script' app.

After creating your Reddit application, rename the file `config.dist.txt` to `config.txt`. Now open `config.txt` using a text editor and replace the following with the correct values:

* `reddit-username`. Username of the Reddit account which will act as a bot
* `reddit-password`. Password of the Reddit account which will act as a bot
* `client-id`. Click edit on your created app, shown underneath `personal use script`
* `client-secret`. Click edit on your created app, shown after `secret`
* `user-agent`. An unique and descriptive identifier. You can read more about the rules [here](https://github.com/reddit-archive/reddit/wiki/API)

After replacing the values and saving `config.txt` the bot is ready to go. It can be run by entering `python bot.py` in the command console.

## Deployment

For deployment of this bot [Heroku](https://www.heroku.com/) was used. Michael Krukov wrote a great tutorial about [hosting a python bot on Heroku](https://github.com/michaelkrukov/heroku-python-script).

## Author

* [Daan den Hartog](http://daandenhartog.com)

You can also contact me via Reddit by sending a PM to [ZemmiphobiaGameDev](https://www.reddit.com/user/ZemmiphobiaGameDev/)