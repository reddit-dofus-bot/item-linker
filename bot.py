from bs4 import BeautifulSoup
from google import google
import requests
import praw
import time
import os

def login():
	if not os.path.isfile('config.txt'):
		print('No config file could be found!')
		print('Please input your credentials in config.dist.txt, and rename it to config.txt')
		return

	config = getFromFile('config')
	reddit = praw.Reddit(username = config[0],
				password = config[1],
				client_id = config[2],
				client_secret = config[3],
				user_agent = config[4])

	return reddit

def getFromFile(filename):
	# If the file doesn't exist, return an empty array
	if not os.path.isfile('{}.txt'.format(filename)):
		return []

	# If the file does exists, we get all the postID's from the file
	with open('{}.txt'.format(filename), 'r') as f:
		data = f.read()
		data = data.split('\n')
		return list(filter(None, data))

def setToFile(filename, data):
	# Create a new file and write the data to this file
	with open('{}.txt'.format(filename), 'w') as f:
		for d in data:
			f.write(d + '\n')

def getItemFromEncyclopedia(url):
	# Create the page using beautiful soup
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')

	# Let the encyclopedia class take care of it
	import encyclopedia as encyclopedia
	return encyclopedia.getItem(soup, url).toString()

def searchGoogle(keywords):
	# Search Google with the following term
	search = 'site:dofus.com {}'.format(keywords)
	results = google.search(search)

	# Return if we didn't get any search results
	if results is None or not results:
		return

	# Return if we don't have a Dofus encyclopedia link
	if 'encyclopedia' not in results[0].link:
		return

	return getItemFromEncyclopedia(results[0].link)

def getItems(keywords):
	output = []

	for i in range(0, len(keywords)):
		result = searchGoogle(keywords[i])
		if result is not None:
			output.append(result)

	return output

def replyToPost(post, text):
	output = '{}{}'.format(text, footnote)
	post.reply(output)

def replyToComment(submission, keywords):
	founditems = getItems(keywords)

	# Don't reply if we don't have any items to link
	if founditems is None or not founditems:
		return

	# Add markdown so the items linked will look good
	reply = founditems[0]
	for i in range(1, len(founditems)):
		reply += '   \n\n{}'.format(founditems[i])

	# Reply to the submission
	replyToPost(submission, reply)

def containsKeywords(comment):
	# See if the submission contains brackets
	# There should be 3 characters between the brackets at minimum
	if '[' in comment and ']' in comment:
		index = comment.index
		if comment.index('[') < comment.index(']') - 3:
			return True

	return False

def containsHyperlink(comment):
	# See if the brackets are maybe connect to a hyperlink
	# This is done for an url between the '(' and ')' characters
	# To prevent further hyperlinks, a check is done to see if the '(' is close to the ']' character
	if '(' in comment and ')' in comment:
		substring = comment.split('(', 1)[1].split(')', 1)[0]
		if '://' in substring and comment.index('(') - 3 < comment.index(']'):
			return True

	return False

def scrapeSubmission(submission, comment):
	# Prevent from replying when the submission is empty, or when a hyperlink is there
	if comment is '' or containsHyperlink(comment) is True:
		return

	# Loop through the submission to find all available keywords
	keywords = []
	while containsKeywords(comment):
		split = comment.split(']', 1)
		keywords.append(split[0].split('[', 1)[1])
		comment = split[1]

	replyToComment(submission, keywords)

def isInList(collection, entry):
	return entry in collection

def isReplyToMe(comment):
	parent = comment.parent()
	if parent is not None:
		return parent.author == reddit.user.me()
	return False

def checkSubmissions(blacklist, submissionsRead):
	resetHasRead = False
	for submission in reddit.subreddit(subreddit).new(limit = submissionLimit):
		# Return if testing is true and the submission is made by anyone else but the testingAccount
		if submission.author != testingAccount and testing is True:
			continue

		# Skip the submission if we already read it
		if isInList(submissionsRead, submission.id) is True:
			continue

		# If the bot hasn't replied yet, scrape the submission to see if we should reply
		if containsKeywords(submission.selftext) and isInList(blacklist, submission.author) is False:
			scrapeSubmission(submission, submission.selftext)

		submissionsRead.append(submission.id)
		resetHasRead = True

	# Reset the read submissions if new ones have been read
	if resetHasRead is True:
		submissionsRead = submissionsRead[len(submissionsRead) - storeLimit:storeLimit]
		setToFile('submissionsRead', submissionsRead)

def checkComments(blacklist, commentsRead):
	resetBlacklist = False
	resetHasRead = False
	for comment in reddit.subreddit(subreddit).comments(limit = commentLimit):
		# Return if testing is true and the comment is made by anyone else but the testingAccount
		if comment.author != testingAccount and testing is True:
			continue

		# Return if we made the comment or already read it
		if comment.author == reddit.user.me() or isInList(commentsRead, comment.id):
			continue

		# See if we need to add/remove users from the blacklist
		if 'ignore me' in comment.body.lower() and isInList(blacklist, comment.author) is False and isReplyToMe(comment) is True:
			print('Adding {} to the blacklist'.format(comment.author))
			replyToPost(comment, 'You have been added to the blacklist, sorry for any inconvenience\n\n')	
			blacklist.append(str(comment.author))
			resetBlacklist = True
		elif 'follow me' in comment.body.lower() and isInList(blacklist, comment.author) is True and isReplyToMe(comment) is True:
			print('Removing {} from the blacklist'.format(comment.author))
			replyToPost(comment, 'You have been removed from the blacklist, welcome back!\n\n')
			blacklist.remove(str(comment.author))
			resetBlacklist = True
		# See if the comment contains an item, and the author is not on the blacklist
		# If both of these are true, see if we already replied to this comment
		elif containsKeywords(comment.body) and isInList(blacklist, comment.author) is False:
			print('Scraping the comment..')
			scrapeSubmission(comment, comment.body)
			
		commentsRead.append(comment.id)
		resetHasRead = True

	# Reset the blacklist if it has been altered
	if resetBlacklist is True:
		commentsRead = commentsRead[len(commentsRead) - storeLimit:storeLimit]
		setToFile('blacklist', blacklist)

	# Reset the read comments if new ones have been read
	if resetHasRead is True:
		setToFile('commentsRead', commentsRead)

def run():
	# Get correct data
	blacklist = getFromFile('blacklist')
	submissionsRead = getFromFile('submissionsRead')
	commentsRead = getFromFile('commentsRead')

	# Check submissions and comments
	checkSubmissions(blacklist, submissionsRead)
	checkComments(blacklist, commentsRead)

	# Restart in sleeptime seconds
	time.sleep(sleeptime)
	run()

# Amount of entries are allowed in submissionsRead/commentsRead
storeLimit = 20

# Amount of submissions/comments which are checked when run is called
submissionLimit = 5
commentLimit = 5

# Amount in seconds the bots sleeps before calling run again
sleeptime = 180

# Used for testing purposes
# When set to True, the bot will only look for submissions/comments made by testingAccount
testingAccount = 'ZemmiphobiaGameDev'
testing = False

subreddit = 'dofus'
footnote = "  \n\n***\n^^I ^^am ^^a ^^bot. ^^To ^^be ^^put ^^on ^^the ^^blacklist ^^reply ^^to ^^this ^^comment ^^with ^^'ignore ^^me'. ^^To ^^be ^^removed ^^from ^^the ^^blacklist ^^reply ^^to ^^this ^^comment ^^with ^^'follow ^^me'. ^^For ^^info ^^you ^^can ^^contact ^^[/u/ZemmiphobiaGameDev](https://www.reddit.com/user/ZemmiphobiaGameDev/)"

reddit = login()
run()