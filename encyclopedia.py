from bs4 import BeautifulSoup
import math

def getItem(soup, url):
	if "/sets/" in url:
		return Set(url, getName(soup), getItemtype(soup), getLevel(soup), getSetParts(soup), getSetEffects(soup))
	if "/equipment/" in url:
		return Equipment(url, getName(soup), getItemtype(soup), getLevel(soup), getDescription(soup), getPartOfSet(soup), getEffects(soup))	
	if "/weapons/" in url:
		return Weapon(url, getName(soup), getItemtype(soup), getLevel(soup), getDescription(soup), getPartOfSet(soup), getEffects(soup), getCharacteristics(soup), getConditions(soup))

def getName(soup):
	div = soup.find('h1', {'class':'ak-return-link'})		
	return ' '.join(div.text.split())

def getDescription(soup):
	div = soup.find('div', {'class':'ak-container ak-panel-stack ak-glue'})
	div = div.find('div', {'class':'ak-panel-content'})
	div = div.find('div', {'class':'ak-panel-content'})
	return ' '.join(div.text.split())

def getLevel(soup):
	div = soup.find('div', {'class':'ak-encyclo-detail-level'})
	return ' '.join(div.text.split())

def getItemtype(soup):
	div = soup.find('div', {'class':'ak-encyclo-detail-type'})
	return ' '.join(div.text.split())

def getPartOfSet(soup):
	for link in soup.find_all('a'):
		if '/sets/' in str(link):
			return ' - Part of [{}]({}{})'.format(link.text, 'https://www.dofus.com', link['href'])

	return '&nbsp;'

def getSetParts(soup):
	div = soup.find('div', {'class':'ak-container ak-panel ak-set-composition ak-nocontentpadding'})
	content = []
	for part in div.find_all('td', {'class':'ak-set-composition-name'}):
		content.append(part.find('a'))

	return content

def getContents(title, soup):
	parent = None
	for div in soup.find_all('div', {'class':'ak-panel-title'}):
		if title in div.text:
			parent = div.parent
			break

	if parent is None:
		return ""

	content = []
	parent = parent.find('div', {'class':'ak-panel-content'})
	for effect in parent.find_all('div', {'class':'ak-title'}):
		content.append(' '.join(effect.text.split()))

	return content

def getEffects(soup):
	return getContents("Effects", soup)

def getSetEffects(soup):
	return getContents("Complete Set Bonus", soup)

def getCharacteristics(soup):
	return getContents("Characteristics", soup)

def getConditions(soup):
	return getContents("Conditions", soup)

class Item(object):
	def __init__(self, url, name, itemtype, level):
		self.url = url
		self.name = name
		self.itemtype = itemtype
		self.level = level

	def getContent(self, content, title):
		if content is None or len(content) is 0:
			return ''

		output = ">\n"
		output += "{} | &nbsp;\n".format(title)
		output += '- | -'

		for i in range(0, math.ceil(len(content) / 2)):
			index = i * 2
			output += '\n{} | {}'.format(content[index], content[index + 1] if index + 1 < len(content) else '&nbsp;')

		return output

	def toString(self):
		return '**[{} ({} {})]({})**'.format(self.name, self.level.replace(':', ''),  self.itemtype.replace('Type: ', ''), self.url)

class Set(Item):
	def __init__(self, url, name, itemtype, level, parts, setbonus):
		super().__init__(url, name, itemtype, level)
		self.parts = parts
		self.setbonus = setbonus

	def getSetPartContent(self, parts):
		output = "Consists of "
		for i in range(0, len(parts)):
			output += "[{}](https://www.dofus.com{})".format(parts[i].text, parts[i]['href'])
			if i < len(parts) - 1:
				output += ', '

		return output
		
	def toString(self):
		return '{}  \n{}  \n{}'.format(super().toString(), self.getSetPartContent(self.parts), super().getContent(self.setbonus, 'Complete set bonus'))

class Equipment(Item):
	def __init__(self, url, name, itemtype, level, description, partofset, effects):
		super().__init__(url, name, itemtype, level)
		self.description = description
		self.partofset = partofset
		self.effects = effects

	def toString(self):
		return '{}{}  \n*{}*\n  \n{}'.format(super().toString(), self.partofset, self.description, super().getContent(self.effects, 'Effects'))

class Weapon(Equipment):
	def __init__(self, url, name, itemtype, level, description, partofset, effects, characteristics, conditions):
		super().__init__(url, name, itemtype, level, description, partofset, effects)
		self.characteristics = characteristics
		self.conditions = conditions

	def toString(self):
		return '{}  \n{}  \n{}'.format(super().toString(), super().getContent(self.characteristics, 'Characteristics'), super().getContent(self.conditions, 'Conditions'))